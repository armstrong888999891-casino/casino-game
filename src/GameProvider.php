<?php

namespace Casino\Game;

use Carbon\Carbon;

interface GameProvider
{
    /**
     * 登入遊戲供應商。
     *
     * @param string $username 用戶名
     *
     * @return string 登入後返回的憑證或狀態
     */
    public function login(string $username): string;

    /**
     * 從遊戲供應商登出。
     *
     * @param string $username 用戶名
     */
    public function logout(string $username): void;

    /**
     * 在遊戲供應商處註冊新用戶。
     *
     * @param string $username 用戶名
     */
    public function register(string $username): void;

    /**
     * 為用戶在遊戲供應商處充值。
     *
     * @param string $username 用戶名
     * @param float  $amount   充值金額
     *
     * @return Transaction 交易詳情
     */
    public function recharge(string $username, float $amount): Transaction;

    /**
     * 從遊戲供應商處提現。
     *
     * @param string $username 用戶名
     * @param float  $amount   提現金額
     *
     * @return Transaction 交易詳情
     */
    public function withdraw(string $username, float $amount): Transaction;

    /**
     * 獲取用戶在遊戲供應商處的餘額。
     *
     * @param string $username 用戶名
     *
     * @return float|int 用戶餘額
     */
    public function getBalance(string $username): float|int;

    /**
     * 獲取特定時間範圍內的賭注列表。
     *
     * @param Carbon $start 查詢起始時間
     * @param Carbon $end   查詢結束時間
     *
     * @return array 賭注列表
     */
    public function getBetList(Carbon $start, Carbon $end): array;

    /**
     * 獲取當前在線的會員列表。
     *
     * @return OnlineMember[] 在線會員列表
     */
    public function getOnlineMemberList(): array;
}
