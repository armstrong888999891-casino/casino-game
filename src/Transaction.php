<?php

namespace Casino\Game;

class Transaction
{
    public function __construct(
        public float $balance,
        public string $transactionId,
    ) {}
}
