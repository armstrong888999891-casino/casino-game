<?php

namespace Casino\Game;

use Illuminate\Foundation\Application;

class GameManager
{
    public function __construct(protected Application $app, protected array $providers) {}

    public function driver(string $name): GameProvider
    {
        if (! isset($this->providers[$name])) {
            throw new \InvalidArgumentException("Game Provider [{$name}] is not defined.");
        }

        $provider = $this->providers[$name];

        if (! isset($provider['driver'])) {
            throw new \InvalidArgumentException("Game Provider [{$name}] driver is not defined.");
        }

        if (! class_exists($provider['driver'])) {
            throw new \InvalidArgumentException("Game Provider [{$name}] driver class [{$provider['driver']}] not found.");
        }

        return $this->app->make($provider['driver'], ['config' => $provider]);
    }
}
