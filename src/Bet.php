<?php

namespace Casino\Game;

use Carbon\Carbon;

class Bet
{
    public function __construct(
        public string $username,
        public int $game_type,
        public int $bet_no,
        public int $round_no,
        public int $bet_amount,
        public int $valid_amount,
        public int $winorloss_amount,
        public string $status,
        public string $currency,
        public array $bet_detail,
        public Carbon $bet_at,
        public Carbon $settle_at,
        public array $origin_response,
    ) {}
}
