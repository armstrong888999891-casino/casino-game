<?php

namespace Casino\Game\Exceptions;

class GameProviderException extends \Exception
{
    public function __construct(string $class, string $function, string $message, int $code = 0)
    {
        parent::__construct("GameProviderException: {$class}::{$function}() - {$message}", $code);
    }
}
