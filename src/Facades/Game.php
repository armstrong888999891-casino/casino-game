<?php

namespace Casino\Game\Facades;

use Casino\Game\GameManager;
use Illuminate\Support\Facades\Facade;

/**
 * @method static \Casino\Game\GameProvider driver(string $name)
 *
 * @see GameManager
 */
class Game extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return GameManager::class;
    }
}
