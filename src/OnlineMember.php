<?php

namespace Casino\Game;

class OnlineMember
{
    public function __construct(
        public string $username,
        public null|int|string $id = null,
    ) {}
}
