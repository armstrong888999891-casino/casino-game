<?php

namespace Casino\Game;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class GameServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(GameManager::class, function (Application $app) {
            return new GameManager($app, $app['config']['game.providers']);
        });
    }
}
